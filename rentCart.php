<?php
class RentCart
{
    public function __construct($id, $brandName, $path, $stock)
    {
      $this->id = $id;
      $this->brandName = $brandName;
      $this->path = $path;
      $this->stock = $stock;
    }

    public function createRentCartPageContent($id, $brandName, $path, $stock)
    {
      $this->id = $id;
      $this->brandName = $brandName;
      $this->path = $path;
      $this->stock = $stock;

      echo '<div class="rent-content">
              <div class="rent-item">
                  <label class="rent-car-brand">Rent '.$brandName.'</p>
              </div>
              <div class="rent-item">
                  <img src='.$path.' alt="loading" class="rent-car-image">
              </div>
              <div class="rent-item">
                <p class="rent-car-stock">Stock :'.$stock.'</p>
              </div>
              <div class="rent-item">
              <form action="cart.php" method="POST">
              <div class="form-item">
              <input type="hidden" name="car-id" class="car-id" value='.$id.'>
              <p class="rent-label">Modes</p>
                    <div class="modes">
                        <input type="radio" class="per--day" name="mode" value="rate_per_day">Per Day
                        <input type="radio" class="per--hour" name="mode" value="rate_per_hour">Per Hour
                        <input type="radio" class="per--km" name="mode" value="rate_per_km">Per Km
                    </div>
              </div>
              <div class="form-item">
                <p class="rent-label">Rent</p>
                    <div class="rent-pricing">
                        <input type="text" class="rent-input rate-inp" value="" required>
                        <input type="number" class="rent-input mode-inp" min="1" value="1" required>
                        <input type="text" class="rent-input price-inp" name="total" value="" readonly>
                    </div>          
                  </div>';
              session_start();
              $user = $_SESSION["username"];
              if($user == NULL)
              {
                  echo '<div class="form-item">
                       <button class="rent-rent-btn-red" disabled>Rent</button><span>You have to signin/register first</span>
                    </div>';
              }
              else
              {
                echo '<div class="form-item">';
                if($stock != "0")
                {  
                echo '<input type="submit" class="rent-rent-btn" name="add-to-cart" value="Rent">
                  </div>';
                }
                else
                {
                  echo '<button class="rent-rent-btn-red" disabled>Rent</button>
                  </div>';
                }
                  
              }
              echo '</form>
              </div>
              </div>';

    }
}

?>