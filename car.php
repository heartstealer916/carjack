<?php
class Car
{
    public function __construct($id, $brandName, $path, $stock)
    {
        $this->id = $id;
        $this->brandName = $brandName;
        $this->path = $path;
        $this->stock = $stock;

    }
    public function createCarBox(string $id, string $brandName,string $path,string $stock)
    {
        $this->id = $id;
        $this->brandName = $brandName;
        $this->path = $path;
        $this->stock = $stock;
        echo '<div class="car-box">
                <div class="car-info">
                    <label class="car-brand">'.$brandName.'</label>
                    <img src='.$path.' alt="no image" class="car-image">
                    <p class="car-stock">Stock : '.$stock.'</p>        
                </div>
                <div class="car-btns">
                    <a href="detailsPage.php?id='.$id.'"><button class="details-btn">Details</button></a>';
                if($stock != "0")
                {
                    echo '<a href="rentCartPage.php?id='.$id.'"><button class="rent-btn">Rent</button></a>
                    </div>';
                }
                else
                { 
                    echo '<a href="rentCartPage.php?id='.$id.'"><button class="rent-btn" disabled>Rent</button></a>
                    </div>';
                }
           echo '</div>';
    }
 
}
?>