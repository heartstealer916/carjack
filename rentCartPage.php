<!DOCTYPE html>
<html lang="en">
<?php
include 'metas.php';
?>
<body>
    <?php
    include 'navbar.php';
    include 'rentCart.php';
    include 'database.php';
    $dao = new Database();
    $nav = new Navbar();
    $cart = new RentCart("","","","","","","");
    ?>
    <div class="cart-container">
            <?php
            if (isset($_GET['id'])) {
                $details = $dao->getCarDetails($_GET['id']);
                foreach ($details as $detail) 
                {
                    $cart->createRentCartPageContent($detail['id'],$detail['name'],$detail['pic'],$detail['stock'],$detail['day'],$detail['hour'],$detail['km']);

                }               
            }
            ?>
    </div>

</body>

</html>