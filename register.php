<!DOCTYPE html>
<html lang="en">
<?php
include 'metas.php';
?>


<body>
    <?php
    include 'navbar.php';
    $nav = new Navbar();
    ?>
    <div class="reg-content">
        <div class="reg-title">
            <h3 class="reg-title-text">Register</h3>
        </div>
        <form action="response.php" method="POST" name="registration" id="reg-form">
            <div class="reg-items">
                <div class="reg-item">
                    <p class="reg-label">Name</p>
                    <div class="item-error">
                    <input type="text" name="name" class="reg-input" placeholder="Full Name" required></div>
                </div>
                <div class="reg-item">
                    <p class="reg-label">Email</p>
                    <div class="item-error">
                    <input type="email" name="email" class="reg-input reg-email" placeholder="Email" required>
                    </div>
                </div>
                <div class="reg-item">
                    <p class="reg-label">User Name</p><div class="item-error">
                    <input type="text" name="username" class="reg-input unique-username" placeholder="Username" required></div>
                </div>
                <div class="reg-item">
                    <p class="reg-label">Password</p><div class="item-error">
                    <input type="password" name="password" class="reg-input" placeholder="Password" required></div>
                </div>
                <div class="reg-item">
                    <p class="reg-label">Street</p><div class="item-error">
                    <input type="text" name="street" class="reg-input" placeholder="Street" required></div>
                </div>
                <div class="reg-item">
                    <p class="reg-label">City</p><div class="item-error">
                    <input type="text" name="city" class="reg-input" placeholder="City" required></div>
                </div>
                <div class="reg-item">
                    <p class="reg-label">State</p><div class="item-error">
                    <input type="text" name="state" class="reg-input" placeholder="State" required></div>
                </div>
                <div class="reg-item">
                    <p class="reg-label">Country</p><div class="item-error">
                    <input type="text" name="country" class="reg-input" placeholder="Country" required></div>
                </div>
                <div class="reg-item">
                    <p class="reg-label">Phone Number</p><div class="item-error">
                    <input type="tel" name="phone" class="reg-input" placeholder="Phone Number" required></div>
                </div>
                <div class="reg-item">
                    <p class="reg-label">Gender</p><div class="item-error">
                        <div class="gender-opt">
                    <input type="radio" class="male" name="gender" value="male">Male
                    <input type="radio" class="female" name="gender" value="female">Female
                    <input type="radio" class="other" name="gender" value="other">Others
                    </div>
                    </div>
                </div>
                <div class="reg-item">
                    <div class="reg-btns">
                        <a href="index.php"><input type="submit" class="reg-cancel" value="Cancel"></a>
                        <input class="reg-register" type="submit" name="register" value="Register">
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>

</html>