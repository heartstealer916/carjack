-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 10, 2021 at 02:19 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `carjack`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `street` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `user_id`, `street`, `city`, `state`, `country`) VALUES
(1, 1, 'street 1', 'RR', 'aaa', 'aaaa'),
(2, 2, 'street 1', 'lahore', 'punjab', 'pakistan'),
(3, 3, 'streeet', 'city', 'state', 'oakistan'),
(4, 3, '', '', '', ''),
(5, 4, 'street 1', 'lahore', 'punjab', 'pakistan');

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

DROP TABLE IF EXISTS `cars`;
CREATE TABLE `cars` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `pic` varchar(225) NOT NULL,
  `info` text NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `name`, `pic`, `info`, `stock`) VALUES
(1, 'Bugatti', 'images/car-1.jpeg', 'The Bugatti Veyron EB 16.4 is a mid-engine sports car, designed and developed in Germany by the Bugatti Engineering GmbH and manufactured by the Bugatti Automobiles SAS in Molsheim, France. Veyron\'s fundamental concept is based on a technical draft of Bugatti chief engineer and \"Technical Guru\" Frank Götzke and it was named after the racing driver Pierre Veyron.\r\n\r\nThe original version has a top speed of 407 km/h (253 mph). It was named Car of the Decade and the best car award (2000–2009) by the BBC television programme Top Gear. The standard Bugatti Veyron also won Top Gear\'s Best Car Driven All Year award in 2005.\r\n\r\nThe SuperSport version of the Veyron is one of the fastest street-legal production cars in the world, with a top speed of 431.072 km/h (267.856 mph). The Veyron Grand Sport Vitesse was the fastest roadster in the world, reaching an averaged top speed of 408.84 km/h (254.04 mph) in a test on 6 April 2013.', 7),
(2, 'Rolls Royce', 'images/car-2.jpeg', 'Rolls-Royce was a British luxury car and later an aero-engine manufacturing business established in 1904 in Manchester, the United Kingdom by the partnership of Charles Rolls and Henry Royce. Building on Royce\'s reputation established with his cranes they quickly developed a reputation for superior engineering by manufacturing the \"best car in the world\". The First World War brought them into manufacturing aero-engines. Joint development of jet engines began in 1940 and they entered production. Rolls-Royce has built an enduring reputation for the development and manufacture of engines for defense and civil aircraft.', 7),
(3, 'Ferrari', 'images/car-3.jpeg', 'Ferrari is an Italian luxury sports car manufacturer based in Maranello, Italy. Founded by Enzo Ferrari in 1939 out of the Alfa Romeo race division as Auto Avio Costruzioni, the company built its first car in 1940 and produced its first Ferrari-badged car in 1947.\r\n\r\nFiat S.p.A. acquired 50% of Ferrari in 1969 and expanded its stake to 90% in 1988. In October 2014, Fiat Chrysler Automobiles (FCA) announced its intentions to separate Ferrari S.p.A. from FCA; as of the announcement FCA owned 90% of Ferrari. The separation began in October 2015 with a restructuring that established Ferrari N.V. (a company incorporated in the Netherlands) as the new holding company of the Ferrari S.p.A. group, and the subsequent sale by FCA of 10% of the shares in an IPO and concurrent listing of common shares on the New York Stock Exchange.', 6),
(4, 'Ferrari', 'images/car-4.jpeg', 'Ferrari is an Italian luxury sports car manufacturer based in Maranello, Italy. Founded by Enzo Ferrari in 1939 out of the Alfa Romeo race division as Auto Avio Costruzioni, the company built its first car in 1940 and produced its first Ferrari-badged car in 1947.\r\n\r\nFiat S.p.A. acquired 50% of Ferrari in 1969 and expanded its stake to 90% in 1988. In October 2014, Fiat Chrysler Automobiles (FCA) announced its intentions to separate Ferrari S.p.A. from FCA; as of the announcement FCA owned 90% of Ferrari. The separation began in October 2015 with a restructuring that established Ferrari N.V. (a company incorporated in the Netherlands) as the new holding company of the Ferrari S.p.A. group, and the subsequent sale by FCA of 10% of the shares in an IPO and concurrent listing of common shares on the New York Stock Exchange.', 7),
(5, 'Jaguar', 'images/car-5.jpeg', 'Jaguar is the luxury vehicle brand of Jaguar Land Rover,a British multinational car manufacturer with its headquarters in Whitley, Coventry, England. Jaguar Cars was the company that was responsible for the production of Jaguar cars until its operations were fully merged with those of Land Rover to form Jaguar Land Rover on 1 January 2013.\r\n\r\nJaguar\'s business was founded as the Swallow Sidecar Company in 1922, originally making motorcycle sidecars before developing bodies for passenger cars. Under the ownership of S. S. Cars Limited, the business extended to complete cars made in association with Standard Motor Co, many bearing Jaguar as a model name. The company\'s name was changed from S. S. Cars to Jaguar Cars in 1945. A merger with the British Motor Corporation followed in 1966, the resulting enlarged company now being renamed as British Motor Holdings (BMH), which in 1968 merged with Leyland Motor Corporation and became British Leyland, itself to be nationalised in 1975.', 6),
(6, 'Lamborghini', 'images/car-6.jpeg', 'Automobili Lamborghini S.p.A. is an Italian brand and manufacturer of luxury sports cars and SUVs based in Sant\'Agata Bolognese. The company is owned by the Volkswagen Group through its subsidiary Audi.\r\n\r\nFerruccio Lamborghini, an Italian manufacturing magnate, founded Automobili Ferruccio Lamborghini S.p.A. in 1963 to compete with established marques, including Ferrari. The company was noted for using a rear mid-engine, rear-wheel-drive layout. Lamborghini grew rapidly during its first decade, but sales plunged in the wake of the 1973 worldwide financial downturn and the oil crisis. The firm\'s ownership changed three times after 1973, including a bankruptcy in 1978. American Chrysler Corporation took control of Lamborghini in 1987 and sold it to Malaysian investment group Mycom Setdco and Indonesian group V\'Power Corporation in 1994. In 1998, Mycom Setdco and V\'Power sold Lamborghini to the Volkswagen Group where it was placed under the control of the group\'s Audi division.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `car_rates`
--

DROP TABLE IF EXISTS `car_rates`;
CREATE TABLE `car_rates` (
  `car_id` int(11) NOT NULL,
  `rate_per_day` int(11) NOT NULL,
  `rate_per_hour` int(11) NOT NULL,
  `rate_per_km` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `car_rates`
--

INSERT INTO `car_rates` (`car_id`, `rate_per_day`, `rate_per_hour`, `rate_per_km`) VALUES
(1, 10000, 2000, 1000),
(2, 15000, 3000, 1500),
(3, 40000, 4000, 2000),
(4, 45000, 4200, 2200),
(5, 35000, 3200, 1800),
(6, 50000, 5000, 2500);

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
CREATE TABLE `transaction` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `mode` enum('day','hour','km') NOT NULL,
  `value` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`id`, `user_id`, `car_id`, `mode`, `value`, `time`) VALUES
(1, 2, 3, 'hour', 4000, '2021-06-10 10:27:22'),
(2, 2, 5, 'km', 1800, '2021-06-10 10:28:32'),
(3, 2, 3, 'hour', 16000, '2021-06-10 10:29:28'),
(4, 2, 1, 'km', 5000, '2021-06-10 10:29:42'),
(5, 3, 1, 'day', 4000, '2021-06-10 10:47:51'),
(6, 3, 1, 'day', 10000, '2021-06-10 10:49:19'),
(7, 4, 3, 'hour', 20000, '2021-06-10 11:53:56');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(225) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `gender` enum('male','female','other') NOT NULL,
  `join_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `phone`, `gender`, `join_time`) VALUES
(1, 'bankist', 'haseebrehmat916@gmail.com', 'session123', '12345', '0234567899', 'male', '2021-06-10 08:12:44'),
(2, 'crud_app', 'haseebrehmat916@gmail.com', 'haseeb', '12345', '0320789567', 'male', '2021-06-10 10:14:15'),
(3, 'awais', 'awaisjavaid93@gmail.com', 'awais', 'awias123', '12345678', 'male', '2021-06-10 10:46:55'),
(4, 'haseeb', 'haseebrehmat916@gmail.com', 'aahil', '12345678', '123456678', 'male', '2021-06-10 11:53:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id_fk2` (`user_id`);

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_rates`
--
ALTER TABLE `car_rates`
  ADD PRIMARY KEY (`car_id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `car_id_fk2` (`car_id`),
  ADD KEY `user_id_fk` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `car_rates`
--
ALTER TABLE `car_rates`
  MODIFY `car_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `user_id_fk2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `car_rates`
--
ALTER TABLE `car_rates`
  ADD CONSTRAINT `car_id_fk1` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `car_id_fk2` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
