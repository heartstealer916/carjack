<?php
class Details
{
    public function __construct($id,$brandName, $path, $ratePerDay, $ratePerHour, $ratePerKm, $info ,$stock)
    {
        $this->id = $id;
      $this->brandName = $brandName;
      $this->path = $path;
      $this->info = $info;
      $this->ratePerDay = $ratePerDay;
      $this->ratePerHour = $ratePerHour;
      $this->ratePerKm = $ratePerKm;
      $this->stock = $stock;
    }

    public function createDetailsPageContent($id, $brandName, $path, $info , $ratePerDay, $ratePerHour, $ratePerKm,$stock)
    {
        $this->id = $id;
      $this->brandName = $brandName;
      $this->path = $path;
      $this->info = $info;
      $this->ratePerDay = $ratePerDay;
      $this->ratePerHour = $ratePerHour;
      $this->ratePerKm = $ratePerKm;
      $this->stock = $stock;

      echo '<div class="det-content">
              <div class="det-item">
                  <label class="det-car-brand">'.$brandName.'</p>
              </div>
              <div class="det-item">
                  <img src='.$path.' alt="loading" class="det-car-image">
              </div>
              <div class="det-item">
                  <p class="det-car-info">'.$info.'</p>
              </div>
              <div class="det-item">
                  <div class="det-car-rates">
                      <div class="rate-per-day">
                          <label class="per-day">Rate Per Date</label>
                          <p class="rate">$'.$ratePerDay.'</p>
                      </div>
                      <div class="rate-per-hour">
                          <label class="per-hour">Rate Per Hour</label>
                          <p class="rate">$'.$ratePerHour.'</p>
                      </div>
                      <div class="rate-per-km">
                          <label class="per-km">Rate Per Km</label>
                          <p class="rate">$'.$ratePerKm.'</p>
                      </div>
                  </div>
              </div>
              <div class="det-item">
                  <p class="det-car-stock">Stock :'.$stock.'</p>
              </div>
              <div class="det-item">';
              if($stock != "0")
              {
                  echo '<a href="rentCartPage.php?id='.$id.'"><button class="det-rent-btn">Rent</button></a>
                  </div>';
              }
              else
              { 
                  echo '<a href="rentCartPage.php?id='.$id.'"><button class="det-rent-btn" disabled>Rent</button></a>
                  </div>';
              }
      echo '</div>';
    }
}

?>