<!DOCTYPE html>
<html lang="en">
<?php
include 'metas.php';
?>


<body>
    <?php
    include 'navbar.php';
    $nav = new Navbar();
    ?>
    <div class="sign-content">
        <div class="sign-title">
            <h3 class="sign-title-text">Sign in</h3>
        </div>
        <form action="response.php" method="POST">
            <div class="sign-items">
                <div class="sign-item">
                    <p class="sign-label">UserName</p>
                    <input type="text" name="username" class="sign-input" placeholder="Username" required>
                </div>
                <div class="sign-item">
                    <p class="sign-label">Password</p>
                    <input type="password" name="password" class="sign-input" placeholder="Password" required>
                </div>

                <div class="sign-item">
                    <div class="sign-btns">
                        <a href="index.php"><input type="submit" class="sign-cancel" value="Cancel"></a>
                        <input type="submit" name="signin" class="sign-signin" value="Sign in">
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>

</html>