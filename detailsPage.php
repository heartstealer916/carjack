<!DOCTYPE html>
<html lang="en">
<?php
include 'metas.php';
?>
<body>
    <?php
    include 'navbar.php';
    include 'details.php';
    include 'database.php';
    $dao = new Database();
    $nav = new Navbar();
    $det = new Details("","","","","","","","");
    ?>
    <div class="details-container">
            <?php
            if (isset($_GET['id'])) {
                $details = $dao->getCarDetails($_GET['id']);
                foreach ($details as $detail) 
                {
                    $det->createDetailsPageContent($detail['id'],$detail['name'],$detail['pic'],$detail['info'],$detail['day'],$detail['hour'],$detail['km'],$detail['stock']);

                }               
            }
            ?>
    </div>

</body>

</html>