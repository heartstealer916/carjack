<!DOCTYPE html>
<html lang="en">
<?php
include 'metas.php'
?>

<body>
    <?php
    session_start();
    include_once 'database.php';
    include 'navbar.php';
    include 'car.php';
    $car = new Car("", "", "", "");
    $dao = new Database();
    $nav = new Navbar();
    // $dao->updateStock("1");
    ?>
    <section class="wrapper">
        <div class="cars-container">
            <?php
            $carsData = $dao->getCarsData();
            if (isset($carsData)) {
                foreach ($carsData as $carData) {
                    $car->createCarBox($carData['id'], $carData['name'], $carData['pic'], $carData['stock']);
                }
            } else {
                echo "<h1>No Cars Available</h1>";
            }
            ?>
        </div>
    </section>
</body>

</html>