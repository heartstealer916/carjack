<?php
class Database
{
    private $connection;
    public function __construct()
    {
        $this->createConnection();
    }
    public function _destruct()
    {
        $this->connection->close();
    }
    private function createConnection()
    {
        $this->connection = new mysqli("localhost", "root", "", "carjack");
        if ($this->connection->connect_error) {
            die("Connection failed" . $this->connection->connect_error . "<br>");
        } else {
            return "Connection made sucessfully" . "<br>";
        }
    }
    public function getCarsData()
    {
        $sql = "select id, name, pic, stock from cars";
        $result = $this->connection->query($sql);
        return $result;
    }
    public function getCarDetails($id)
    {
        $sql = "Select c.id, c.name, c.pic, c.info, c.stock, cr.rate_per_day as day, cr.rate_per_hour as hour, cr.rate_per_km as km
                from
                cars c join car_rates cr
                on
                c.id = cr.car_id where c.id = '$id' ";
        $result = $this->connection->query($sql);
        return $result;
    }
    public function addUser($name, $email, $username, $password, $street, $city, $state, $country, $phone, $gender)
    {

        $sql1 = "INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `phone`, `gender`, `join_time`) 
                VALUES (NULL, '$name', '$email', '$username', '$password', '$phone', '$gender', current_timestamp())";
        $this->connection->query($sql1);

        $sql2 = "select id from users order by id desc limit 1";
        $result1 = $this->connection->query($sql2);
        foreach ($result1 as $value) {
            $user_id = $value['id'];
        }
        $sql3 = "INSERT INTO `address` (`id`, `user_id`, `street`, `city`, `state`, `country`) 
                VALUES (NULL, '$user_id', '$street', '$city', '$state', '$country')";
        $result2 =  $this->connection->query($sql3);

        return $result2 ? "data inserted" : $this->connection->error;
    }
    public function validateUser($username, $password): bool
    {
        $sql = "select (count(id) > 0) as userExists from users where username = '$username' and password = '$password'";
        $result = $this->connection->query($sql);
        foreach ($result as $value) {
            $exists = $value['userExists'];
        }
        if ($exists == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function checkIfUsernameExists($username): bool
    {
        $sql = "select (count(id) > 0) as userExists from users where username = '$username'";
        $result = $this->connection->query($sql);
        foreach ($result as $value) {
            $exists = $value['userExists'];
        }
        if ($exists == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getIdByUsername($username)
    {
        $sql = "select id from users where username = '$username'";
        $result = $this->connection->query($sql);
        foreach ($result as $value) {
            $id = $value['id'];
        }
        return $id;
    }
    public function getRateByIDAndMode($id,$mode)
    {
        $sql = "select {$mode} from car_rates where car_id = {$id}";
        $result = $this->connection->query($sql);
        foreach ($result as $value) {
            $rate = $value[$mode];
        }
        return $rate;
    }

    public function updateStock($id)
    {
        $sql = "UPDATE cars SET stock = stock - 1 WHERE id = '$id'";
        $result = $this->connection->query($sql);
    }
    public function addTransaction($user_id, $car_id, $mode, $price)
    {
        $sql = "INSERT INTO `transaction` (`id`, `user_id`, `car_id`, `mode`, `value`,`time`) VALUES (NULL,'$user_id','$car_id','$mode','$price',current_timestamp())";
        $result = $this->connection->query($sql);
    }
}
