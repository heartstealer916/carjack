<?php
include 'database.php';
$dao = new Database();
if (isset($_POST['register'])) {
    session_start();
    $_SESSION["username"] = $_POST['username'];
    $dao->addUser(
        $_POST['name'],
        $_POST['email'],
        $_POST['username'],
        $_POST['password'],
        $_POST['street'],
        $_POST['city'],
        $_POST['state'],
        $_POST['country'],
        $_POST['phone'],
        $_POST['gender']
    );
    header("Location: index.php") or die();
}
if (isset($_POST['signin'])) 
{
    $flag = $dao->validateUser($_POST['username'], $_POST['password']);
    if ($flag) {
        session_start();
        $_SESSION['username'] = $_POST['username'];
        header("Location: index.php") or die();
    } 
    else 
    {
        echo "<script>alert('invalid credentials');
        window.location.href='signin.php';</script>";
    }
}
