<?php
class Navbar
{
    public function __construct()
    {
        $this->createNavbar();
    }
    private function createNavbar()
    {
        echo '<div class="navbar">
        <div class="left">
            <div class="nav-item">
                <div class="nav-logo">
                    <a href="index.php"><img src="./images/icons8_car_1.ico" alt="" class="image">
    
                    </a>
                    <p class="brand-name">carjack</p>
                </div>
            </div>
    
            <div class="nav-item">
                <ul><a href="index.php">Home</a></ul>
            </div>
        </div>
        <div class="right">';

        session_start();
        $user = $_SESSION["username"];
        if ($user == NULL) {
            $this->createRegisterAndSigninBtn();
        } else {
            $this->createLogoutBtn($user);
        }
    }

    private function createRegisterAndSigninBtn()
    {
        echo '<div class="nav-item">
            <a href="register.php"><button class="register-btn">Register</button></a>

            </div>
            <div class="nav-item">
            <a href="signin.php"><button class="signin-btn">Sign In</button></a>

        </div>
        </div>
        </div>';
    }
    private function createLogoutBtn($user)
    {
        echo '<div class="nav-item">
            <img class="user-icon" src="images/user.png" alt="loading" width="8%" height="5%">
            </div>
            <div class="nav-item">
            <label class="user-label">'.$user.'</label>
            </div>
            <div class="nav-item">
            <a href="logout.php"><button class="logout-btn">Log out</button></a>
           </div>
           </div>
           </div>';
    }
}
