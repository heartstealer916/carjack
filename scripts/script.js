
$(".per--day,.per--hour,.per--km").on('change', function ()
{
    const mode = $(this).val();
    const id = $(".car-id").val();
    $.ajax({
        type: 'POST',
        url: "price.php",
        data: 'mode=' + mode + '&car_id=' + id,
        success: function (value)
        {
            value = +value;
            $('.rate-inp').val("");
            $('.rate-inp').val(value);
        }
    });
});

$(".mode-inp").on('change click', function ()
{
    $('.price-inp').val('');
    let val1 = $('.rate-inp').val();
    let val2 = parseInt($(this).val());
    let value = val1 * val2;
    $('.price-inp').val(value);

});

$(".unique-username").focusout(function ()
{
    $('.unique-username').css("color", "green");
    $('.unique-username').css("background-color", "white");
    const val = $(this).val();
    $.ajax({
        type: 'POST',
        url: "username.php",
        data: 'check_username=' + val,
        success: function (flag)
        {
            console.log(typeof flag);
            if (flag == "1")
            {

                $('.unique-username').css("background-color", "#eb958f");
                $('.unique-username').val('');
                $('.unique-username').attr("placeholder", "Try with different username");
            }

        }
    });

});
// $("form[name='registration']").validate({
$(document).ready(function ()
{
    $("#reg-form").validate({
        rules: {
            name: "required",
            username: "required",
            street: "required",
            city: "required",
            state: "required",
            country: "required",
            gender: "required",
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 8
            }
        },
        messages: {
            name: "Please enter your name",
            username: "Enter valid username",
            street: "Please enter your street",
            city: "Please enter your city",
            state: "Please enter your state",
            country: "Please enter your country",
            gender: "Select your gender",
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 8 characters long"
            },
            email: "Please enter a valid email address"
        },
        submitHandler: function (form)
        {
            form.submit();
        }
    });
});
