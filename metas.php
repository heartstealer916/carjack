<?php
echo '<head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="styles/style.css">
        <link rel="stylesheet" href="styles/register.css">
        <link rel="stylesheet" href="styles/signin.css">
        <link rel="stylesheet" href="styles/details.css">
        <link rel="stylesheet" href="styles/rentCart.css">
        <script defer src="scripts/jquery.js"></script>
        <script defer src="scripts/jquery.validate.min.js"></script>
        <script defer src="scripts/script.js" type="text/javascript"></script>
        <title>Car Jack</title>
    </head> ';
